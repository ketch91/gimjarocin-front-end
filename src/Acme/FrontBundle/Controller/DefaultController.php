<?php

namespace Acme\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('AcmeFrontBundle:Default:index.html.twig', array());
    }
    public function newsAction()
    {
        return $this->render('AcmeFrontBundle:Default:news.html.twig', array());
    }
    public function showarticleAction($id)
    {
        return $this->render('AcmeFrontBundle:Default:article.html.twig', array('id' => $id));
    }
    public function historyAction()
    {
        return $this->render('AcmeFrontBundle:Default:history.html.twig', array());
    }
    public function staffAction()
    {
        return $this->render('AcmeFrontBundle:Default:staff.html.twig', array());
    }
    public function planAction()
    {
        return $this->render('AcmeFrontBundle:Default:plan.html.twig', array());
    }
    public function documentsAction()
    {
        return $this->render('AcmeFrontBundle:Default:documents.html.twig', array());
    }
    public function interestsAction()
    {
        return $this->render('AcmeFrontBundle:Default:interests.html.twig', array());
    }
    public function galleryAction()
    {
        return $this->render('AcmeFrontBundle:Default:gallery.html.twig', array());
    }
    public function contactAction()
    {
        return $this->render('AcmeFrontBundle:Default:contact.html.twig', array());
    }
}
